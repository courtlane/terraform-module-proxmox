#!/bin/bash

# Get the directory of the script
script_dir=$(dirname "$0")

# Decrypt the .env file via ansible-vault
echo "Decrypting .env file using Ansible-Vault..."
ansible-vault decrypt "$script_dir/.env" --output "$script_dir/.env.decrypted"

# Load environment variables
set -a
source "$script_dir/.env.decrypted"
set +a

# Remove the decrypted file
rm "$script_dir/.env.decrypted"
