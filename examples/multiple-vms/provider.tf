terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox"
      version = "3.0.1-rc1"
    }
  }
  # Optional: Minio S3 remote state config
  # Important: Replace <YOUR_PROJECT_NAME> and <YOUR_ENVIRONMENT> For Example: "terraform-services/hashicorp-vault/prod/terraform.tfstate"
  # Check that this path is not used by another project in Minio S3
  #backend "s3" {
  #  bucket                      = "terraform-proxmox"
  #  key                         = "<UNIQUE_S3_BUCKET_PATH>/terraform.tfstate"
  #  endpoints                   = {
  #    s3                        = "https://minio.example.com:9000"
  #  }
  #  region                      = "<S3_REGION>"
  #  skip_credentials_validation = true
  #  skip_requesting_account_id  = true
  #  skip_metadata_api_check     = true
  #  skip_region_validation      = true
  #  use_path_style              = true
  #} 

  # Optional: GitLab remote backend state config
  # Ensure the <GITLAB_PROJECT_ID> is set and the <TF_STATE_FILENAME> at the end of the addresses is unique
  # if you manage multiple state files in one GitLab repository.
  #backend "http" {
  #  address        = "https://gitlab.com/api/v4/projects/<GITLAB_PROJECT_ID>/terraform/state/<TF_STATE_FILENAME>"
  #  lock_address   = "https://gitlab.com/api/v4/projects/<GITLAB_PROJECT_ID>/terraform/state/<TF_STATE_FILENAME>/lock"
  #  unlock_address = "https://gitlab.com/api/v4/projects/<GITLAB_PROJECT_ID>/terraform/state/<TF_STATE_FILENAME>/lock"
  #  lock_method    = "POST"
  #  unlock_method  = "DELETE"
  #  retry_wait_min = 5

  #  # Important: Set the GitLab `Username` and `Personal Access Token` as environment variables
  #  # export TF_HTTP_USERNAME='courtlane'
  #  # export TF_HTTP_PASSWORD='<PERSONAL_ACCESS_TOKEN>'
  #}
}

provider "proxmox" {
  pm_api_url      = "https://192.168.178.10:8006/api2/json"
  pm_tls_insecure = true
  pm_timeout      = 600
  pm_parallel     = 4

  # Important: Set the Proxmox `PM_USER` and `PM_PASS` credentials as environment variables
  #export PM_USER='terraform-prov@pve'
  #export PM_PASS='<PASSWORD>'
}

