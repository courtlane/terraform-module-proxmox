# Advanced Example: Deploy multiple virtual machines with vm_count and start_ip variables 
variable "vm_count" {
  type    = number
  default = 3
}

variable "start_ip" {
  type    = number
  default = 31
}

module "podman" {
  source = "git::https://gitlab.com/courtlane/terraform-module-proxmox.git?ref=v1.1.3"
  count  = var.vm_count

  proxmox_node = "<PROXMOX_NODE>"
  hostname     = "podman-${format("%02d", count.index + 1)}"
  domainname   = "example.com"
  vmid         = "178${format("%03d", count.index + var.start_ip)}"
  tags         = "podman;terraform"
  desc         = <<EOT
    Podman VM
    Node ${count.index + 1}/${var.vm_count}
EOT
  clone_image  = "debian-12-cloudinit-template"
  network_ip   = "192.168.178.${count.index + var.start_ip}"
  network_gw   = "192.168.178.1"
  cores        = "2"
  sockets      = "1"
  memory       = "2048"
  disk_size    = "20"
  disk_storage = "nvme"
  additional_disks = {
    "disk1" = {
      size    = "10"
      storage = "nvme"
      backup  = true
      cache   = "none"
      discard = true
    }
  }
}
