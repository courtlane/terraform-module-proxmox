# Simple Example: Deploy a single virtual machines

module "docker" {
  source = "git::https://gitlab.com/courtlane/terraform-module-proxmox.git?ref=v1.1.3"

  proxmox_node = "<PROXMOX_NODE>"
  hostname     = "docker-01"
  domainname   = "example.com"
  vmid         = "101"
  tags         = "docker;terraform"
  desc         = <<EOT
    Docker Single VM
EOT
  clone_image  = "ubuntu-24.04-cloudinit-template"
  network_ip   = "192.168.1.20"
  network_gw   = "192.168.1.1"
  cores        = "2"
  sockets      = "1"
  memory       = "2048"
  disk_size    = "20"
  disk_storage = "nvme"

  # Optional: additional disks
  additional_disks = {
    "disk1" = {
      size    = "10"
      storage = "nvme"
      backup  = true
      cache   = "none"
      discard = true
    }
    "disk2" = {
      size    = "5"
      storage = "nvme"
      backup  = true
      cache   = "none"
      discard = true
    }
  }
}
