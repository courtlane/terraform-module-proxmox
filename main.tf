# Create VM Resource 
resource "proxmox_vm_qemu" "proxmox_vm" {

  name        = "${var.hostname}.${var.domainname}"
  vmid        = var.vmid
  desc        = var.desc
  tags        = var.tags
  target_node = var.proxmox_node
  clone       = var.clone_image
  os_type     = var.os_type
  qemu_os     = var.qemu_os
  bios        = var.bios
  cores       = var.cores
  sockets     = var.sockets
  cpu         = var.cpu
  memory      = var.memory
  scsihw      = var.scsihw
  boot        = var.boot
  onboot      = var.onboot
  full_clone  = var.full_clone
  agent       = var.agent

  disks {
    virtio {
      virtio0 {
        disk {
          size    = local.root_disk_size
          storage = var.disk_storage
          backup  = var.disk_backup
          cache   = var.disk_cache
          discard = var.disk_discard
        }
      }
      dynamic "virtio1" {
        for_each = { for i, disk in var.additional_disks : i => disk if i == "disk1" }
        content {
          disk {
            size    = virtio1.value["size"]
            storage = virtio1.value["storage"]
            backup  = virtio1.value["backup"]
            cache   = virtio1.value["cache"]
            discard = virtio1.value["discard"]
          }
        }
      }
      dynamic "virtio2" {
        for_each = { for i, disk in var.additional_disks : i => disk if i == "disk2" }
        content {
          disk {
            size    = virtio2.value["size"]
            storage = virtio2.value["storage"]
            backup  = virtio2.value["backup"]
            cache   = virtio2.value["cache"]
            discard = virtio2.value["discard"]
          }
        }
      }
      dynamic "virtio3" {
        for_each = { for i, disk in var.additional_disks : i => disk if i == "disk3" }
        content {
          disk {
            size    = virtio3.value["size"]
            storage = virtio3.value["storage"]
            backup  = virtio3.value["backup"]
            cache   = virtio3.value["cache"]
            discard = virtio3.value["discard"]
          }
        }
      }
      dynamic "virtio4" {
        for_each = { for i, disk in var.additional_disks : i => disk if i == "disk4" }
        content {
          disk {
            size    = virtio4.value["size"]
            storage = virtio4.value["storage"]
            backup  = virtio4.value["backup"]
            cache   = virtio4.value["cache"]
            discard = virtio4.value["discard"]
          }
        }
      }
    }
  }

  network {
    model  = var.network_model
    bridge = var.network_bridge
    tag    = var.network_tag
  }

  # Cloud Init Settings
  cloudinit_cdrom_storage = var.disk_storage
  ipconfig0               = "ip=${var.network_ip}/${var.network_mask},gw=${var.network_gw}"
  ciuser                  = var.ciuser
  cipassword              = var.cipassword
  searchdomain            = var.searchdomain != "" ? var.searchdomain : var.domainname
  nameserver              = var.nameserver
  sshkeys                 = fileexists("~/.ssh/id_ed25519.pub") ? file("~/.ssh/id_ed25519.pub") : file("~/.ssh/id_rsa.pub")

  # Ignore changes to vm parameters
  lifecycle {
    ignore_changes = [
      sshkeys,
      cloudinit_cdrom_storage,
      boot
    ]
  }

  # Remove Cloud Init drive and reboot virtual machine to finalize deletion of cdrom.
  provisioner "local-exec" {
    command = <<-EOF
      echo "Removing Cloud Init drive and rebooting VM if necessary..."
      if [[ "${var.delete_cloud_init_drive}" == "true" && -n "${var.proxmox_ssh_host}" ]]; then
        ssh -o BatchMode=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o LogLevel=quiet root@${var.proxmox_ssh_host} 'pvesh set /nodes/${var.proxmox_node}/qemu/${var.vmid}/config --delete ide2 && pvesh set /nodes/${var.proxmox_node}/qemu/${var.vmid}/config --delete ide3 && qm reboot ${var.vmid}'
      fi
    EOF    
  }
}
