# terraform-module-proxmox <!-- omit in toc -->
This module automates the deployment of virtual machines, allowing for the provisioning of a single VM or multiple VMs simultaneously.

Follow the steps below to integrate it into your project repository and customize it according to your requirements.

# Table of contents <!-- omit in toc -->
- [Prerequisites](#prerequisites)
- [Usage](#usage)
  - [Backend Configuration (Optional)](#backend-configuration-optional)
  - [Virtual Machine Configuration](#virtual-machine-configuration)
  - [Secrets](#secrets)
    - [Encrypt Secrets using Ansible-Vault (Optional)](#encrypt-secrets-using-ansible-vault-optional)
  - [Deploy VM](#deploy-vm)


## Prerequisites

In this example, I'm using a Proxmox user called `terraform-prov@pve`, which was created based on the documentation provided by [`telmate/proxmox`](https://registry.terraform.io/providers/Telmate/proxmox/latest/docs)
and [Gitlab-managed Terraform state](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html) as a backend for the Terraform state.

The Terraform state contains sensitive content, and losing it could lead to discrepancies between the actual and desired states of the VMs. 
Therefore, it's highly recommended to securely store it in S3 object storage.

You can follow the examples provided for `Minio` or `Gitlab-managed Terraform state` in the `provider.tf` file.
However, feel free to skip this step if you prefer not to store the Terraform state remotely.

This module utilizes cloud image templates that must be created in your Proxmox cluster prior to deployment.
Explore the provided Ansible role below for assistance with this task, or refer to other documentation available online.
- [ansible-role-proxmox-cloud-templates](https://gitlab.com/courtlane/ansible-role-proxmox-cloud-templates.git)

## Usage

Clone this repository and copy the contents of the `examples` directory in your project

```bash
git clone https://gitlab.com/courtlane/terraform-module-proxmox.git /tmp/terraform-module-proxmox/
```

Copy one of the provided examples (e.g. `single-vm` or `multiple-vms`) into your project directory 

```bash
cp -r /tmp/terraform-module-proxmox/examples/single-vm ~/my_project
cd ~/my_project
```

### Backend Configuration (Optional)

Modify the backend configuration in `provider.tf` or skip this step if you prefer not to store the Terraform state remotely

```hcl
...
# Example Gitlab-managed state backend
backend "http" {
  address        = "https://gitlab.com/api/v4/projects/583285/terraform/state/tf_state"
  lock_address   = "https://gitlab.com/api/v4/projects/583285/terraform/state/tf_state/lock"
  unlock_address = "https://gitlab.com/api/v4/projects/583285/terraform/state/tf_state/lock"
  lock_method    = "POST"
  unlock_method  = "DELETE"
  retry_wait_min = 5
}
...
```

### Virtual Machine Configuration 

Modify the VM configuration in `main.tf` 

Replace `<PROXMOX_NODE>` with the proxmox server where you want the VM deploy to. Do not use the FQDN! The value should match a Proxmox node as they are shown in the Web-UI

Customize all VM parameters in the `main.tf` file according to your requirements.

You can find both required and optional input variables in the [`variables.tf`](https://gitlab.com/courtlane/terraform-module-proxmox/-/blob/master/variables.tf?ref_type=heads) file of the module.

### Secrets 

Select one of the following methods to manage the necessary credentials in this project:

Export the necessary secrets in your shell environment:

```bash
# Gitlab credentials for remote state
export TF_HTTP_USERNAME='courtlane'
export TF_HTTP_PASSWORD='<PERSONAL_ACCESS_TOKEN>'

# Proxmox credentials 
export PM_USER='terraform-prov@pve'
export PM_PASS='<PROMXOX_PASSWORD>'
```

#### Encrypt Secrets using Ansible-Vault (Optional)

Use [Ansible-Vault](https://docs.ansible.com/ansible/latest/vault_guide/vault_encrypting_content.html) to create and encrypt the secrets stored in the `.env` file. Encrypting the file enables you to securely store it in Git.

```bash
# Create .env file
touch .env
```

**Contents of `.env` file::**
```md
export TF_HTTP_USERNAME='<GITLAB_USERNAME>'
export TF_HTTP_PASSWORD='<PERSONAL_ACCESS_TOKEN>'
export PM_USER='<PROXMOX_USERNAME>'
export PM_PASS='<PROMXOX_PASSWORD>'
```

**Encrypt with Ansible-Vault**
```bash
ansible-vault encrypt .env
```

The `tf_env_secrets.sh` script manages the decryption of the `.env` file, sources the secret variables into the current shell, and removes the decrypted file.

```bash
source ./tf_env_secrets.sh
```

### Deploy VM
```bash
terraform init
terraform plan
terraform apply
```
