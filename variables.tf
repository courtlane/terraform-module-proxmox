# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ---------------------------------------------------------------------------------------------------------------------

variable "proxmox_node" {
  type        = string
  description = "Proxmox node name where the VM will be deployed (should match the display name in the Proxmox UI)"
  default     = ""
}

variable "proxmox_ssh_host" {
  type        = string
  description = "Resolvable DNS hostname or IP address of the Proxmox server. This variable is required only if 'delete_cloud_init_drive' is set to true. The 'delete_cloud_init_drive' provisioner tasks require an SSH connection to the host to perform their work."
  default     = ""
}

variable "hostname" {
  type        = string
  description = "Virtual machine hostname"
}

variable "domainname" {
  type        = string
  description = "Primary DNS domain name"
}

variable "vmid" {
  type        = string
  description = "VM identifier within proxmox"
}

variable "clone_image" {
  type        = string
  description = "Cloud Init template name which should be cloned"
}

variable "disk_storage" {
  type        = string
  description = "Datastore where the virtual disks will be deployed on"
}

variable "network_ip" {
  type        = string
  description = "IP Address in CIDR notation ipconfig0 device"
}

variable "network_gw" {
  type        = string
  description = "Gateway Address on ipconfig0 device"
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters have reasonable defaults.
# ---------------------------------------------------------------------------------------------------------------------

variable "desc" {
  type        = string
  description = "Virtual machine description notes"
  default     = ""
}

variable "tags" {
  type        = string
  description = "Virtual machine tags"
  default     = "terraform"
}

variable "cores" {
  type        = string
  description = "Amount of cpu cores"
  default     = "1"
}

variable "sockets" {
  type        = string
  description = "Amount of cpu sockets"
  default     = "1"
}

variable "memory" {
  type        = string
  description = "Amount of virtual machine memory"
  default     = "2048"
}

variable "disk_size" {
  type        = number
  description = "Amount of disk space in GB on the first virtual disk"
  default     = 20
}

variable "disk_backup" {
  type        = bool
  description = "Whether proxmox should backup the primary disk or not."
  default     = true
}

variable "disk_cache" {
  type        = string
  description = "Whether proxmox should cache disk IO or redirect it to the storage immediately."
  default     = "none"
  #default     = "directsync" # To-DO none or directsync?
}

variable "disk_discard" {
  type        = bool
  description = "Whether to pass discard/trim requests to the underlying storage (Thin provisioning)"
  default     = true
}

variable "swap" {
  type        = number
  description = "Swapfile in GB, which will be created on the root partition"
  default     = 0
}

variable "additional_disks" {
  type        = map(any)
  description = "A disk block used to configure additional disk devices"
  default     = {}
}

variable "network_model" {
  type        = string
  description = "Model of the network adapter of the first NIC"
  default     = "virtio"
}

variable "network_bridge" {
  type        = string
  description = "Name of the network bridge where the VM (first NIC) will be connected to"
  default     = "vmbr0"
}

variable "network_tag" {
  type        = string
  description = "VLAN tag on the fist VM network adapter"
  default     = "-1"
}

variable "os_type" {
  type        = string
  description = "Which provisioning method to use, based on the OS type. Possible values: ubuntu, centos, cloud-init."
  default     = "cloud-init"
}

variable "qemu_os" {
  type        = string
  description = "Specify guest operating system. This is used to enable special optimization/features for specific operating systems"
  default     = "l26"
}

variable "bios" {
  type        = string
  description = "The BIOS to use, options are seabios or ovmf for UEFI. Defaults to seabios"
  default     = "seabios"
}

variable "cpu" {
  type        = string
  description = "The type of CPU to emulate in the Guest"
  default     = "host"
}

variable "scsihw" {
  type        = string
  description = "The SCSI controller to emulate"
  default     = "virtio-scsi-pci"
}

variable "boot" {
  type        = string
  description = "Boot order"
  default     = "order=virtio0"
}

variable "onboot" {
  type        = bool
  description = "Boot VM after Proxmox starts"
  default     = true
}

variable "full_clone" {
  type        = bool
  description = "Whether create a full or linked clone"
  default     = true
}

variable "agent" {
  type        = number
  description = "Set to 1 to enable the QEMU Guest Agent"
  default     = 1
}

variable "network_mask" {
  type    = string
  default = "24"
}

variable "ciuser" {
  type        = string
  description = "User that will be created with Cloud Init"
  default     = "root"
}

variable "cipassword" {
  type        = string
  description = "Password for ciuser that will be created with Cloud Init"
  sensitive   = true
  default     = "InitialPassword"
}

variable "delete_cloud_init_drive" {
  type        = bool
  description = "Specifies whether to delete the Cloud Init drive after the VM has been successfully created, or to keep it."
  default     = false
}

variable "searchdomain" {
  type        = string
  description = "Search domain(s) to be configured by Cloud Init at first boot. If unset, it defaults to var.domainname."
  default     = ""
}

variable "nameserver" {
  type        = string
  description = "Nameservers that will be configured by Cloud Init at first boot"
  default     = "192.168.178.2 192.168.178.1"
}
